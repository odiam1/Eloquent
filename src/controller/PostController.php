<?php

namespace Eloquent\Controller;

require_once __DIR__ . '/Controller.php';
require_once __DIR__ . '/UserController.php';
require_once __DIR__ . '/../service/constants/Constants.php';

use Eloquent\Service as Constants;

class PostController extends Controller{

    public function __construct(){
        parent::__construct();
    }

    public function __destruct(){
        parent::__destruct();
    }

    public function addPost(string $author, string $content){
        $q = sprintf(Constants\Constants::$DB_QUERIES['insert_new_post'], $author, $content);
        return $this->connector->insert_query($q);
    }

    public function removePost(int $authorID){}

    public function getHomePosts(){ 
        $q = Constants\Constants::$DB_QUERIES['home_posts'];
        return $this->connector->select_query($q);
     }

    public function getProfilePosts(string $username){

    }

}