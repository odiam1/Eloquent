<?php
namespace Eloquent\Controller;


ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);


require_once __DIR__ . "/../service/MySqlService.php";
use Eloquent\Service as S;

abstract class Controller{
    protected $connector = null;

    protected function __construct(){
        $this->connector = new S\MySqlService(FALSE);
    }

    protected function __destruct(){
        if($this->connector != NULL)
            $this->connector->close();
    }

}
