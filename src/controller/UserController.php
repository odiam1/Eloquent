<?php
namespace Eloquent\Controller;
require_once __DIR__ . '/../service/MySqlService.php';
require_once __DIR__ . '/../service/constants/Constants.php';
require_once __DIR__ . '/Controller.php';

use \Eloquent\Service as Constant;
use \Eloquent\Controller as Abstr;

class UserController extends Abstr\Controller{


    public function __destruct(){
       parent::__destruct();
    }

    public function __construct(){
        parent::__construct();
    }

    /**
     * Inserts a new user, still don't know if psw should
     * be already hashed.
     */
    public function register($username, $password){
        // Checks if the user already exists
        if($this->getUserByUsername($username)) return FALSE;
        
        // Builds the query
        $hp = password_hash($password, PASSWORD_BCRYPT);
        $q = sprintf(Constant\Constants::$DB_QUERIES['insert_user'],
                      $username,
                      password_hash($password, PASSWORD_BCRYPT));
        
        //executes the query and returns the result (true/false)
        return $this->connector->insert_query($q);
    }


    
    private function getUserByUsername($username){
        // Build the query
        $q = sprintf(Constant\Constants::$DB_QUERIES['check_user'], $username);
        // Submits the query
        $user = $this->connector->select_query($q);
        // Returns the result
        return $user;
    }

    /**
     * Checks if input username and password match any user in the db
     * @return boolean [tells if user and pass match]
     */
    public function login($username, $password) : bool{
        $user = $this->getUserByUsername($username);
        if($user && count($user) == 1){
            echo "USER BEFORE CHECK";
            var_dump($password);
            return password_verify($password, $user[0]['password']);
        }
        return FALSE;
    }


    /**
     * Gets the id of a user given his username.
     * I wanted to use it when I made the post table to get the
     * author's username, but I found anothery way through SQL...
     * @return int id
     */
    public function getUserIdByUsername(string $username){}

}
?>