<?php session_start(); ?>

<html>
<head>
<link rel="stylesheet" href="css/basicstyle.css">
<script src="js/jquery.js"></script>

</head>
<body>
    
    <?php

    if(isset($_GET["logout"])){
        $_SESSION["logged"] = NULL;
        $_SESSION["username"] = NULL;

    }
    if(isset($_SESSION["logged"]) && isset($_SESSION["username"])){
            echo 'ALREADY LOGGED AS <b>' . $_SESSION["username"] . '</b>';

    }else if(isset($_POST["username"]) && isset($_POST["password"])){

            include '../controller/UserController.php';

            $userCtrl = new \Eloquent\Controller\UserController();
            $res = $userCtrl->login($_POST["username"], $_POST["password"]);
            
            if($res){
                echo "LOGGED!<br>";
                $_SESSION["logged"] = TRUE;
                $_SESSION["username"] = $_POST["username"];
                header("Location:home.php");
            }else{
                echo "User doesn't exists";
            }
    
    }else{?>

        
        <div class="container">
        <div class="form">
        <form action="login.php" method="post">
            Username:
            <input type="text" name="username" ><br>
            Password:
            <input type="password" name="password" ><br>
            <input type="submit" value="LOGIN">
            
        </form>

        <a href="register.php"><button class="register">REGISTRATI</button></a>
        </div>
        
        </div>
        
    <?php } ?>
</body>
</html>