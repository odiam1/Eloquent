<?php

include __DIR__ . '/../../controller/PostController.php';
use Eloquent\Controller as Contr;

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

if(isset($_POST["options"])){
    
    header("Content-Type: application/json");
    
    $postController = new Contr\PostController();
    $res = null;
    switch($_POST['options']){
        case 'home_posts':
            $res = $postController->getHomePosts();
            break;
        
        case 'profile_posts':
            $res = $postController->getProfilePosts($user);
            break;
        
        case 'add_post':
            if( isset($_POST["author"]) && isset($_POST["content"]) )
                $res = $postController->addPost($_POST["author"], $_POST["content"]);
            else
                $res = false;
            break;


        default:
            $res = ["error" => 404, "message" => "Option not valid."];

    }

    

    //DEBUG ONLY
}else{
    $res = ["error" => 404, "message" => "Option not valid."];
}

echo json_encode($res);