let HOST = "localhost"; // for post requests



function buildPost(author, content, likes, time){
    
    return $("<div class=\"post\"></div>").append(
            $("<div class=\"author-name\"></div>").text(author),
            $("<div class=\"post-content\"></div>").text(content),
            //$("<div class=\"buttons\"></div>").text(author),
            $("<hr></hr>")
        );
}

$(document).ready(
    function(){
        /**
         * Handles the click event on "Share" button 
         */
        var url = "http://"+HOST+"/api/posts.php";
        $("#create-post").click(function(){

            $.post(url,
            {
                'options':'add_post',
                'author':$("#post-author").val(),
                'content':$("#post-content").val(),
            },

            function(data, status){
                console.log(data, status);
                if(status === 'success'){
                    $("div.home-posts").prepend(
                        buildPost(
                            $("#post-author").val(),
                            $("#post-content").val(),
                            0,
                            0
                        )
                    );
                    $("#post-content").val("");
                }
            }
            );
        });


        $.post(url,

            {'options':'home_posts'},

            function(data, status){
                console.log(data);
                data.forEach(x =>$("div.home-posts").append(
                    buildPost(x.author, x.content, x.likes, x.time))
                );
            }
        );


    }
);

function buildPost(author, content, likes, time){

    return $("<div class=\"post\"></div>").append(
            $("<div class=\"author-name\"></div>").text(author),
            $("<div class=\"post-content\"></div>").text(content),
            $("<div class=\"buttons\"></div>").append(
              $("<p></p>").text(likes),
              $("<button class=\"comment\"></button>").text("Comment"),

            ),
            $("<hr></hr>")
        );
}

/* function buildPost(author, content, likes, time){
    $("div.home-posts").append(
        $("<div class=\"post\"></div>").append(
            $("<div class=\"author-name\"></div>").text(author),
            $("<div class=\"post-content\"></div>").text(content),
            //$("<div class=\"buttons\"></div>").text(author),
            $("<hr></hr>")
        )
    );
} */

function addNewPost(author, content){

}
