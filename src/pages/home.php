<?php session_start() ?>
<html>
<head>
<link rel="stylesheet" href="css/basicstyle.css" />
<link rel="stylesheet" href="css/home.css" />
</head>
<body>
    
    <div class="container">
        
        <?php
        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
        
        // if user is logged
         if(isset($_SESSION['username']) && isset($_SESSION["logged"]) && $_SESSION["logged"]){ ?>

        <span class="side-icons">
            <button id="profile-button"><img src="images/profile.png" alt="Profile" class="icon" /></button>
         </span>

        <div class="profile-header">
            Logged in as <b><?php echo $_SESSION['username']; ?></b>  <a href="login.php?logout=1" >EXIT</a>
        </div>

        <div class="new-post form">
            <form action="#">
            <textarea rows=6 cols=40 id="post-content"></textarea><br>
            <input type="hidden" id="post-author" value="<?php echo $_SESSION['username'] ?>" />
            <button id="create-post">Convidivi</button>
            </form>
        
        </div>

        <?php
         }else{
             header("Location:login.php");
         } ?>
        
        
        <br><br><hr>


        <!-- HOME POSTS -->
        <?php
            require_once __DIR__ . '/../controller/PostController.php';
            use Eloquent\Controller as C;
            $conn = new C\PostController();
            $posts = $conn->getHomePosts();
        ?>
        
        <div class="home-posts">
            
        </div>
    </div>
    <script src="js/jquery.js"></script>
    <script src="js/home.js"></script>
</body>
</html>